package com.student_registration.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.TreeMap;

import com.student_registration.models.StudentMarks;
import com.student_registration.models.StudentModel;
import com.student_registration.utilities.DataBaseConnector;
import com.student_registration.utilities.UtilityDao;

/**
 * this class is used for All the CURD operations on student file
 * 
 * @author Group B
 *
 */
public class StudentDbconnection {

	
	
	
	
	
	/**
	 * 
	 * This method deletes the student information in all tables..
	 * @param studentId
	 * @return bollean 
	 */
	public boolean deleteStudentById(String studentId){
		boolean status = false;
		Connection con = DataBaseConnector.getConnection();
		
			 try {
				 
				PreparedStatement ps = con.prepareStatement("delete from student_table where studentId =?");
				PreparedStatement ps1 = con.prepareStatement("delete from student_authentication_table where studentId =?");
				PreparedStatement ps2 = con.prepareStatement("delete from student_marks where student_id = ?");
				ps.setString(1,studentId);
			    int deletedRows =	ps.executeUpdate();
			    ps1.setString(1,studentId);
			    int deletedRows1 =	ps1.executeUpdate();
			    ps2.setString(1,studentId);
			    ps2.executeUpdate();
			   
			    if(deletedRows > 0 && deletedRows1 > 0 ) {
					status = true ;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
			 return status;
		

		
	}
	/**
	 * this method regers the new student into the StudentInfo file
	 * 
	 * @param StudentModel Object
	 * @return true if the student is regestered else return false
	 */

	@SuppressWarnings("finally")
	public boolean registerStudent(StudentModel student) {
		boolean status = false;
		Connection con = DataBaseConnector.getConnection();
		PreparedStatement ps;
		try {
		 ps = con.prepareStatement("insert into student_authentication_table values(?,?,?)");
				ps.setString(1, student.getStudentId());
				ps.setString(2, "Inno@123");
	            ps.setString(3, "myCollage");
	            int authResult = ps.executeUpdate();
	       
	        ps= con.prepareStatement("INSERT INTO student_table VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
			ps.setString(1, student.getStudentId());
			ps.setString(2, student.getFirstName());
			ps.setString(3, student.getLastname());
			ps.setString(4, student.getDateOfBirth());
			ps.setString(5, student.getMobileNo());
			ps.setString(6, student.getEmail());
			ps.setString(7, student.getGender());
			ps.setString(8, student.getQualification());
			ps.setString(9, student.getCaste());
			ps.setString(10, student.getCourse());
			ps.setInt(11, student.getJoiningYear());
			ps.setString(12, student.getAddress());
			int dataResult = ps.executeUpdate();
			int feesResult = updateFees(student.getStudentId(), student.getFees());
			
				
			if ( authResult > 0 && dataResult > 0 && feesResult > 0) {
				status = true;
			}

		} catch (SQLException e) {
			return false;
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return status;
		}

		
	}


	/**
	 * this method check user details in the data base
	 * 
	 * @param userName
	 * @param password
	 * @return
	 */

	public StudentModel studentLogin(String userName, String password) {

		Connection con = DataBaseConnector.getConnection();
		StudentModel student = null;
		PreparedStatement ps;
		try {

			ps = con.prepareStatement("select * from student_authentication_table where studentId = ? and password = ?");
			ps.setString(1, userName);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				String studentId = rs.getString(1);
				student = new UtilityDao().fetchStudentDetails(studentId);
			}

		} catch (Exception e) {
            e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return student;
	}
	
	

	/**
	 * This method takes the studentId and securityQuestion and validtaes weather user is valid or not.
	 *  If valid gives chance to reset password for user 
	 * @param studentId
	 * @param securityQue
	 * @return
	 */
	
	
	public String studentForgotPassWord(String studentId, String securityQue) {
	
		
		Connection con = DataBaseConnector.getConnection();
		try {
			
			PreparedStatement ps = con
					.prepareStatement("select * from student_authentication_table where studentId = ? and security_que = ?");
			ps.setString(1, studentId);
			ps.setString(2, securityQue);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
			 return studentId;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	
	public boolean resetStudentPassword(String newPassWord,String studentId,String favouritePlace) {
		Connection con = DataBaseConnector.getConnection();
		boolean status = false;
		try {
			PreparedStatement ps = con.prepareStatement("update student_authentication_table set password = ?, security_que = ? where studentId = ?");
			ps.setString(1, newPassWord);
			ps.setString(2, favouritePlace);
			ps.setString(3, studentId);
		
			int res = ps.executeUpdate();
			if (res > 0)
				status = true;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return status;

	}


	

	/**
	 * this method updates the student data in the database param
	 * @param studentId,fieldname,value
	 * @return boolean 
	 */
	public boolean updateStudentField(String studentId, String fieldName, String value) {
		Connection con = DataBaseConnector.getConnection();
		Boolean status = false;
		try {
			PreparedStatement ps = con.prepareStatement("update student_table set " + fieldName + " = ? where studentId = ?" );
			ps.setString(1, value);
			ps.setString(2, studentId);
			int updatedRows = ps.executeUpdate();
			if (updatedRows > 0) {
				status = true;
			}

		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return status;

	}

	/**
	 * this method checks the last student id and returns incremented value if that
	 * 
	 * @return studentName
	 */

	public String getNewStudentId() throws Exception {

		Connection con = DataBaseConnector.getConnection();
		String newStudentId = "CN0010001";
		try {
			if (con == null) {
				throw new Exception("sorry some thing went wrong");
			}

			Statement statment = con.createStatement();
			ResultSet rs = statment.executeQuery("select studentId from student_table order by studentId desc limit 1");

			if (rs.next()) {
				String lastStudentId = rs.getString(1);
				int postFix = Integer.parseInt(lastStudentId.substring(4)) + 1;
				newStudentId = "CN00" + postFix;
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			con.close();
		}
		return newStudentId;

	}

	
	/**
	 * this method inserts the fees details into student_fees_details table
	 * 
	 * @studentId,fees
	 * @int inserted rows count
	 */

	public int updateFees(String studentId, double fees) {
		Connection con = DataBaseConnector.getConnection();
		int updatedRows = 0;
		try {
			PreparedStatement ps = con.prepareStatement("insert into student_fees_details values(?,?,?)");
			Date dt = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String currentTime = sdf.format(dt);
			ps.setString(1, studentId);
			ps.setString(2, currentTime);
			ps.setDouble(3, fees);

			updatedRows = ps.executeUpdate();

		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return updatedRows;
	}

	/**
	 * this method updates the marks into the student_marks table
	 * 
	 * @param student TreeMap<String,Marks>
	 * @TreeMap<String,Marks>
	 **/

	public int updateMarks(TreeMap<String, StudentMarks> marksList, String studentId) {
		Connection con = DataBaseConnector.getConnection();
		int updatedSems = 0;
		try {
			PreparedStatement ps = con.prepareStatement("insert into student_marks values(?,?,?,?,?,?,?,?,?)");
			Set<String> keys = marksList.keySet();
            
			for (String key : keys) {
				StudentMarks marks = marksList.get(key);
				ps.setString(1, studentId);
				ps.setString(2, key);
				ps.setInt(3, marks.getSubject1());
				ps.setInt(4, marks.getSubject2());
				ps.setInt(5, marks.getSubject3());
				ps.setInt(6, marks.getSubject4());
				ps.setInt(7, marks.getSubject5());
				ps.setInt(8, marks.getSubject6());
				ps.setInt(9, marks.getSemBacklogs());

				 if(ps.executeUpdate() > 0) {
					 updatedSems++;
				 }
			}

		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return updatedSems;
	}
	
	
	
	/**
	 * this method reads the marks from the student_marks table 
	 * @param studentId 
	 * @TreeMap<String,StudentMarks>
	 */
	
	public TreeMap<String, StudentMarks> readMarks(String studentId) {
		Connection con = DataBaseConnector.getConnection();
	    TreeMap<String, StudentMarks> marksList= new TreeMap<String,StudentMarks>();
		try {
		
           PreparedStatement ps= con.prepareStatement("select * from student_marks  where student_id = ?");
           ps.setString(1, studentId);
			ResultSet rs = ps.executeQuery();
            
			while (rs.next()) {
			     StudentMarks marks = new  StudentMarks();
			     marks.setSubject1(rs.getInt(3));
			     marks.setSubject2(rs.getInt(4));
			     marks.setSubject3(rs.getInt(5));
			     marks.setSubject4(rs.getInt(6));
			     marks.setSubject5(rs.getInt(7));
			     marks.setSubject6(rs.getInt(8));
			     marks.setSemBacklogs(rs.getInt(9));
			     marksList.put(rs.getString(2), marks);
			}
		} catch (Exception e) {	
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return marksList;
	
	}
}
