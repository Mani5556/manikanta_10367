package com.student_registration.utilities;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.TreeMap;
import com.student_registration.models.StudentModel;

/**
 * this class is having the common file handlings
 * 
 * @author IMVIZAG
 *
 */
public class UtilityDao {

	/**
	 * This method to fetch details of student by Id
	 * 
	 * 
	 */
	public StudentModel fetchStudentDetails(String studentId) {
		StudentModel student = null;
		Connection con = DataBaseConnector.getConnection();
		PreparedStatement ps;
		PreparedStatement ps1;
		try {
		   
			ps = con.prepareStatement("select * from student_table where studentId = ?");
			ps.setString(1, studentId);
			
			ResultSet rs = ps.executeQuery();
			
			ps1 = con.prepareStatement("select sum(paid_fees) from student_fees_details where studentId = ?");
			ps1.setString(1, studentId);
			ResultSet rs1 = ps1.executeQuery();
			double paidFees = 0 ;
			
			if(rs1.next())
				paidFees = rs1.getDouble(1);
		   
			student = fillStudent(rs,paidFees);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return student;

	}

	/**
	 * this method returns the fillled student object
	 * 
	 * @param rs
	 * @return studentModel object
	 */
	public StudentModel fillStudent(ResultSet rs,double paidFees) {
		StudentModel student = null;
		
		try {
			
			 while(rs.next()) {
				student = new StudentModel();
				student.setStudentId(rs.getString("studentId"));
				student.setFirstName(rs.getString("firstName"));
				student.setLastname(rs.getString("Lastname"));
				student.setDateOfBirth(rs.getString("dob"));
				student.setMobileNo(rs.getString("MobileNo"));
				student.setEmail(rs.getString("Email"));
				student.setAddress(rs.getString("address"));
				student.setGender(rs.getString("Gender"));
				student.setQualification(rs.getString("Qualification"));
				student.setCaste(rs.getString("Caste"));
				student.setCourse(rs.getString("Course"));
				student.setJoiningYear(rs.getInt("JoiningYear"));
				student.setFees(paidFees);
				student.setBacklogs(getTotalBackLogs(student.getStudentId()));
                student.calculateDueAndSet(student.getCaste(),student.getFees());
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return student;
	}
	
	
	/**
	 * this method get the total backlogs of the student 
	 */
	
	public int getTotalBackLogs(String studentId) {
		Connection con = DataBaseConnector.getConnection();
		int backLogs = 0;
		try {
			PreparedStatement ps = con.prepareStatement("select SUM(sem_Wise_Backlogs) as total_backlogs from student_marks WHERE student_id = ?");
			ps.setString(1, studentId);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				backLogs = rs.getInt("total_backlogs");
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return backLogs;
	}
	
	
	/**
	 * this method gets the fee details of given studentId
	 * @param String studentId
	 * @return Student Details 
	 */
	public TreeMap<String,Double> getFeesDetails(String studentId) {
		Connection con = DataBaseConnector.getConnection();
		TreeMap<String,Double> feeDetail = new TreeMap<String,Double>();
		try {
			PreparedStatement ps = con.prepareStatement("select paid_date,paid_fees from student_fees_details where studentid = ?");
			ps.setString(1, studentId);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				String date = rs.getTimestamp(1).toString();
				feeDetail.put(date,rs.getDouble(2) );
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return feeDetail;
	}
	
}
