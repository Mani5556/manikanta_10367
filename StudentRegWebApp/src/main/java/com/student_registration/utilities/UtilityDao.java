package com.student_registration.utilities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.TreeMap;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.student_registration.models.StudentModel;

/**
 * this class is having the common file handlings
 * 
 * @author IMVIZAG
 *
 */
public class UtilityDao {

	SessionFactory sf = null;
	Session session = null;
	Transaction transaction = null;

	public UtilityDao() {
		sf = DataBaseConnector.getSessionFactory();
		session = sf.openSession();
		transaction = session.beginTransaction();
	}

	/**
	 * This method to fetch details of student by Id
	 * 
	 * 
	 */
	public StudentModel fetchStudentDetails(String studentId) {
		StudentModel student = null;
		student = session.get(StudentModel.class, studentId);
		transaction.commit();
		Query query = session.createQuery("select sum(sf.paidFees) as totalfee from StudentFeesModel sf  where studentId = :studentId");
		query.setParameter("studentId", studentId);
		double fees = (double) query.uniqueResult();
		student = fillStudent(student, fees);
		return student;

	}

	/**
	 * this method returns the fillled student object
	 * 
	 * @param rs
	 * @return studentModel object
	 */
	public StudentModel fillStudent(StudentModel student, double paidFees) {

		student.setFees(paidFees);
		student.setBacklogs(getTotalBackLogs(student.getStudentId()));
		student.calculateDueAndSet(student.getCaste(), student.getFees());

		return student;
	}

	/**
	 * this method get the total backlogs of the student
	 */

	public int getTotalBackLogs(String studentId) {
		Connection con = DataBaseConnector.getConnection();
		int backLogs = 0;
		try {
			PreparedStatement ps = con.prepareStatement(
					"select SUM(sem_Wise_Backlogs) as total_backlogs from student_marks WHERE student_id = ?");
			ps.setString(1, studentId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				backLogs = rs.getInt("total_backlogs");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return backLogs;
	}

	/**
	 * this method gets the fee details of given studentId
	 * 
	 * @param String studentId
	 * @return Student Details
	 */
	public TreeMap<String, Double> getFeesDetails(String studentId) {
		Connection con = DataBaseConnector.getConnection();
		TreeMap<String, Double> feeDetail = new TreeMap<String, Double>();
		try {
			PreparedStatement ps = con
					.prepareStatement("select paid_date,paid_fees from student_fees_details where studentid = ?");
			ps.setString(1, studentId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				String date = rs.getTimestamp(1).toString();
				feeDetail.put(date, rs.getDouble(2));

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return feeDetail;
	}

}
