package com.student_registration.utilities;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.student_registration.models.AdminModel;
import com.sun.javafx.collections.MappingChange.Map;

public  class JsonDeserializer {
	
	/**
	 * this method coverts the given json String into 
	 * @param jsonString
	 * @return
	 */
	public static <T> T getObject(String jsonString,Class<T> type){
	   Gson gson = new Gson(); 
	   T obj = gson.fromJson(jsonString,type);
	   return obj;
	   
	}
	
	public static <T> Map<String,T> getMap(String jsonString,Class<T> type){
		   Gson gson = new Gson(); 
		   Type mapType = new TypeToken<Map<String, T>>(){}.getType();	
		   Map<String,T> obj = gson.fromJson(jsonString,mapType);
		   return obj ;
		}
	public static <T> ArrayList<T> getList(String jsonString,Class<T> type){
		   Gson gson = new Gson(); 
		   Type mapType = new TypeToken<ArrayList<T>>(){}.getType();	
		   ArrayList<T> obj = gson.fromJson(jsonString,mapType);
		   return obj ;
    }
}
