package com.student_registration.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "posts_table")
public class PostModel {
	@Id
	int postId;
	Date date;
	String title;
	@Column(name = "image_src")
	String imageSrc;
	String message;
     
	/**
	 * @return the id
	 */
	public int getPostId() {
		return postId;
	}

	/**
	 * @param id the id to set
	 */
	public void setPostId(int id) {
		this.postId = id;
	}

	/**
	 * @return the imageSrc
	 */
	public String getImageSrc() {
		return imageSrc;
	}

	/**
	 * @param imageSrc the imageSrc to set
	 */
	public void setImageSrc(String imageSrc) {
		this.imageSrc = imageSrc;
	}

	/**
	 * 
	 * 
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the imagSrc
	 */
	public String getImagSrc() {
		return imageSrc;
	}

	/**
	 * @param imagSrc the imagSrc to set
	 */
	public void setImagSrc(String imagSrc) {
		this.imageSrc = imagSrc;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

}
