package com.student_registration.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
@Entity
@Table(name = "admin_table")
public class AdminModel {
	
	@Transient
	private String uniqueId;
	
	@Id
	private int empId;

	@Column(name = "userName")
	private String adminUserName;
	
	@Column(name = "password")
	private String adminPassword;
	@Transient
	private String adminReEnterPassword;
	@Column(name = "securityQue")
	private String adminSecurityQue;

	
	
	
	
	
	/**
	 * @return the empId
	 */
	public int getEmpId() {
		return empId;
	}

	/**
	 * @param empId the empId to set
	 */
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	
	public String getAdminSecurityQue() {
		return adminSecurityQue;
	}

	public void setAdminSecurityQue(String adminSecurityQue) {
		this.adminSecurityQue = adminSecurityQue;
	}

	public String getAdminReEnterPassword() {
		return adminReEnterPassword;
	}

	public void setAdminReEnterPassword(String adminReEnterPassword) {
		this.adminReEnterPassword = adminReEnterPassword;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getAdminUserName() {
		return adminUserName;
		
	}

	public void setAdminUserName(String adminUserName) {
		this.adminUserName = adminUserName;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	
	}
}
