package com.student_registration.models;

import java.util.ArrayList;

public class AdminHomeModel {
   ArrayList<RequestModel> requests;
   /**
 * @return the requests
 */
public ArrayList<RequestModel> getRequests() {
	return requests;
}
/**
 * @param requests the requests to set
 */
public void setRequests(ArrayList<RequestModel> requests) {
	this.requests = requests;
}
/**
 * @return the admin
 */
public AdminModel getAdmin() {
	return admin;
}
/**
 * @param admin the admin to set
 */
public void setAdmin(AdminModel admin) {
	this.admin = admin;
}
AdminModel admin;
}
