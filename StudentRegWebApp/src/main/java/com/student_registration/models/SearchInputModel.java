package com.student_registration.models;

import java.util.ArrayList;

import com.student_registration.enums.EnumClasses;

public class SearchInputModel {
  private  EnumClasses.SearchKeys searchContext;
  private String input;
  private ArrayList<StudentModel> searchList;
/**
 * @return the searchContext
 */
public EnumClasses.SearchKeys getSearchContext() {
	return searchContext;
}
/**
 * @param searchContext the searchContext to set
 */
public void setSearchContext(EnumClasses.SearchKeys searchContext) {
	this.searchContext = searchContext;
}
/**
 * @return the input
 */
public String getInput() {
	return input;
}
/**
 * @param input the input to set
 */
public void setInput(String input) {
	this.input = input;
}
/**
 * @return the searchList
 */
public ArrayList<StudentModel> getSearchList() {
	return searchList;
}
/**
 * @param searchList the searchList to set
 */
public void setSearchList(ArrayList<StudentModel> searchList) {
	this.searchList = searchList;
}
}
