package com.student_registration.webservices;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.student_registration.dao.StudentDbconnection;
import com.student_registration.models.LoginInputModel;
import com.student_registration.models.MarksInputModel;
import com.student_registration.models.RequestModel;
import com.student_registration.models.SearchInputModel;
import com.student_registration.models.StudentHomeModel;
import com.student_registration.models.StudentMarks;
import com.student_registration.models.StudentModel;
import com.student_registration.services.StudentService;
import com.student_registration.utilities.JsonDeserializer;

@Path("/student")
public class StudentWebServices {
	StudentService studentService = new StudentService();

	Gson gson = new Gson();

	StudentDbconnection studentDbconnection = new StudentDbconnection();

	/**
	 * This method registers the student with student personal details
	 * 
	 * @param studentId
	 * @param fees
	 * @return
	 */
	@PUT
	@Path("/registerStudent")
	@Produces("Application/json")
	public String registerStudent(String studentJson) {
		StudentModel student = JsonDeserializer.getObject(studentJson, StudentModel.class);
		Boolean result = studentService.registerStudent(student);
		return gson.toJson(result);
	}

	/**
	 * this method authenticates the student details before login
	 * 
	 * @param userName
	 * @param password
	 * @return
	 */
	@POST
	@Path("/login")
	@Produces("Application/json")
	public String login(String LoginJson) {
	    LoginInputModel loginData = JsonDeserializer.getObject(LoginJson, LoginInputModel.class);
		StudentService studentService = new StudentService();
		StudentHomeModel studentHomeModel = studentService.studentLogin(loginData.getUserName(), loginData.getPassword());
		if (studentHomeModel != null) {
			Gson gson = new Gson();

			String adminJson = gson.toJson(studentHomeModel);

			return adminJson;
		} else {
			return null;
		}
	}

	/**
	 * This method used to recover the forget password of the student
	 * 
	 * @param studentId
	 * @param newPassword
	 * @param favPlace
	 * @return
	 */
	@GET
	@Path("/forgotPassword/{id}/{securityQuestion}")
	@Produces("Application/json")
	public String forgotPassword(@PathParam("id") String studentId,
			@PathParam("securityQuestion") String securityQuestion) {
		StudentService studentService = new StudentService();
		String studentPk = studentService.studentForgotPassWord(studentId, securityQuestion);
		return new Gson().toJson(studentPk);
	}

	/**
	 * This method used to reset the student Password
	 * 
	 * @param studentId
	 * @param newPassword
	 * @param favPlace
	 * @return
	 */
	@PUT
	@Path("/resetPassword/{id}/{newPassword}/{favPlace}")
	@Produces("Application/json")
	public String resetPassword(@PathParam("id") String studentId,
			@PathParam("newPassword") String newPassword, @PathParam("favPlace") String favPlace) {

		Boolean status = studentService.resetPassword(studentId, newPassword, favPlace);
		return status.toString();

	}

	/**
	 * This method deletes the Existing student details with the help of student Id
	 * 
	 * @param studentId
	 * @return
	 */
	@GET
	@Path("/deleteStudent/{id}")
	@Produces("Application/json")
	public String deleteStudent(@PathParam("id") String studentId) {
		Boolean status = studentService.deleteStudent(studentId);
		return status.toString();

	}

	/**
	 * This method updates the student Details
	 * 
	 * @param studentId
	 * @param fieldName
	 * @param value
	 * @return
	 */
	@GET
	@Path("/updateStudent/{id}/{fieldName}/{value}")
	@Produces("Application/json")
	public String updateStudent(@PathParam("studentId") String studentId, @PathParam("fieldName") String fieldName,
			@PathParam("value") String value) {
		Boolean status = studentService.updateStudent(studentId, fieldName, value);
		return status.toString();

	}

	@POST
	@Path("/search")
	@Produces("Application/json")
	public String search(String searchInputJson) {
		// converting the input json into SearchInputModel type
		SearchInputModel searchInput = JsonDeserializer.getObject(searchInputJson, SearchInputModel.class);
		StudentService studentService = new StudentService();
		System.out.println(searchInput.getSearchContext() + " " + searchInput.getInput());
		// getting the data from the database
		ArrayList<StudentModel> students = studentService.search(searchInput.getSearchContext(), searchInput.getInput(),
				searchInput.getSearchList());
		Gson gson = new Gson();
		return gson.toJson(students);

	}
	
	
	
	
	

	/**
	 * This method update the student Marks
	 * 
	 * @param studentId
	 * @param fees
	 * @return
	 */
	@PUT
	@Path("/updateMarks")
	@Produces("Application/json")
	public String updateMarks(String marksInputJson) {
		MarksInputModel marksInput = JsonDeserializer.getObject(marksInputJson, MarksInputModel.class);
		int result = studentService.updateMarks(marksInput.getStudentMarksList(), marksInput.getStudentId());
		if (result > 0) {
			return gson.toJson(true);

		} else {
			return gson.toJson(false);
		}
	}

	/**
	 * This method calls the student service readmarks method and fetch the marks of
	 * a student;
	 * 
	 * @param studentId
	 * @return
	 */
	@GET
	@Path("/readMarks/{id}")
	@Produces("Application/json")
	public String readMarks(@PathParam("id") String studentId) {
		System.out.println(studentId);
		List<StudentMarks> studentMarksList = studentService.readMarks(studentId);
		if (studentMarksList != null) {
			return gson.toJson(studentMarksList);
		} else {
			return gson.toJson(false);
		}
	}

	/**
	 * This method updates the student fees based on studentId
	 * 
	 * @param studentId
	 * @param fees
	 * @return
	 */
	@GET
	@Path("/updateFees/{id}/{fees}")
	@Produces("Application/json")
	public String updateFees(@PathParam("id") String studentId, @PathParam("fees") double fees) {
		Boolean status = studentService.updateFees(studentId, fees);
		return status.toString();
	}

	/**
	 * this method calls the database and gives the next student id to the last
	 * studentId present db
	 * 
	 * @return String studentId
	 * @throws Exception
	 */
	@GET
	@Path("/getNewStudentId")
	public String getNewStudentId() {
		return studentService.getNewStudentId();
	}

	/**
	 * this method getting the fees details of the perticuller student
	 * 
	 * @param studentId
	 * @return Map of fees and dates
	 */
	@GET
	@Path("/feesDetails/{id}")
	@Produces(MediaType.APPLICATION_JSON)

	public String getFeesDetails(@PathParam("id") String studentId) {
		TreeMap<String, Double> feesList = studentService.getFeesDetails("CN0010001");
		return gson.toJson(feesList);
	}
	
	/**
	 * This method is used for the raising the complant to the admin 
	 */
	
	@PUT 
	@Path("/studentRequest")
	public String studentRequests(String requestJson) {
		RequestModel request = JsonDeserializer.getObject(requestJson, RequestModel.class);
		
		Boolean status = studentService.studentRequests(request);
		return status.toString();
	}
	
}
