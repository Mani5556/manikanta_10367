


package com.abc.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.abc.hibernate.entities.Account;

@Repository
public class AccountDaoImpl implements AccountDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Account searchAccountById(int Id) {
		Session session = sessionFactory.getCurrentSession();
		Account acc = session.get(Account.class, Id);
		return acc;
	}

	@Override
	public boolean createAccount(Account acc) {
		Session session = sessionFactory.getCurrentSession();
		int accNo = (int) session.save(acc);
		return true;
	}

	@Override
	public boolean deleteAccount(int accNo) {
		Session session = sessionFactory.getCurrentSession();
		Account acc = session.get(Account.class, accNo);
		if (acc != null) {
			session.delete(acc);
			return true;
		}
		else
			return false;
	}

	@Override
	public boolean updateAccount(Account acc) {
		Session session = sessionFactory.getCurrentSession();
		Account account = session.get(Account.class, acc.getAccno());
	
		if(account != null) {
		 session.merge(acc);
		   return true;
		}
		else {
			return false;
		}
	}

}
