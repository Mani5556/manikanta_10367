package com.abc.dao;

import com.abc.hibernate.entities.Account;

public interface AccountDao {
   public Account searchAccountById(int Id);
   public boolean createAccount(Account acc);
   public boolean deleteAccount(int accNo);
   public boolean updateAccount(Account acc);
  
}
