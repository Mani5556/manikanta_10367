package com.abc.servieces;



import org.omg.CORBA.PRIVATE_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.dao.AccountDao;
import com.abc.hibernate.entities.Account;
@Service
public class AccountServieceImpl implements AccountService {
    @Autowired
	private AccountDao accountDao;
    
    @Transactional
	@Override
	public Account searchAccountByid(int id) {
	    return accountDao.searchAccountById(id);
		
	}
    @Transactional
	@Override
	public boolean createAccount(Account acc) {
		return accountDao.createAccount(acc);
	}
	@Override
    @Transactional
	public boolean deleteAccount(int accNo) {
		return accountDao.deleteAccount(accNo);
	}
	@Transactional
	@Override
	public boolean updateAccount(Account acc) {
		return accountDao.updateAccount(acc);
	}
}
