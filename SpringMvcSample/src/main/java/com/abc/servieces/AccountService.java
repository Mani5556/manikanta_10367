package com.abc.servieces;

import com.abc.hibernate.entities.Account;

public interface AccountService {
 public Account searchAccountByid(int id);
 public boolean createAccount(Account acc);
 public boolean deleteAccount(int accNo);
 public boolean updateAccount(Account acc);
}
