package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.abc.hibernate.entities.Account;
import com.abc.servieces.AccountService;

@Controller
public class AccountController {
	@Autowired
	private AccountService accountService;

	@GetMapping("/search")
	public String getAccountById(@RequestParam int accNO, ModelMap map) {
		Account acc = accountService.searchAccountByid(accNO);
		map.addAttribute("modelData", acc);
		return "account_saved";
	}

	@PostMapping("/create")
	public String createAccount(@ModelAttribute Account acc,ModelMap map) {
		
		
		String modelData = "";
		if (accountService.createAccount(acc)) {
			modelData = "Account created SuccessFully";
		} else {
			modelData = "Account creation failed";
		}
		map.addAttribute("modelData", modelData);
		return "message";

	}
	
	@GetMapping("/deleteAccount")
	public String deleteAccount(@RequestParam int accNo, ModelMap map) {
		boolean status = accountService.deleteAccount(accNo);
		String msg = status ? "account deleted successfully":"deletion failed";
		map.addAttribute("modelData", msg);
		return "message";
	}
	
	@PostMapping("/updateAccount")
	public String deleteAccount(@ModelAttribute Account acc,ModelMap map) {
		boolean status = accountService.updateAccount(acc);
		String msg = status ? "account updated successfully":"Oops updtation failed";
		map.addAttribute("modelData", msg);
		return "message";
	}

}
