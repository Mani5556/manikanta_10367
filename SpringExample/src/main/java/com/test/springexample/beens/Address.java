package com.test.springexample.beens;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Address {
 
   @Value("${student.city}") 
   private String city;
  
   @Value("${student.state}")
   private String state;

 public String getCity() {
	return city;
	             
}
public void setCity(String city) {
	this.city = city;
}
public String getState() {
	return state;
}
public void setState(String state) {
	this.state = state;
}
   
}
