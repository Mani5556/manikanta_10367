<%@page import="com.sun.xml.internal.bind.v2.schemagen.xmlschema.Import"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="com.sample.bean.*,java.util.*" %>
<%@ include file="home.html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body >
	<c:if test="${studentsList.size() == 0}">
	     <jsp:forward page="Error.jsp" ></jsp:forward>
	 </c:if>
	 
	 <c:if test="${studentsList.size() > 0}">
	     
	    <center style="margin-top: 100px">
           <table border="1" style="width: 40%;background-color:rgba(10,10, 0, 0.5);text-align: center ;color: white;">
           <tr style="color: blue;"><th>Id</th><th>name</th><th>age</th></tr>
           <c:forEach var="student" items="${studentsList}">
              <tr><td>${student.id}</td><td>${student.name}</td><td>${student.age}</td></tr>
           </c:forEach>

          </table>
        </center>
	</c:if>

 </body>
</html>