package com.sample.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class UpdateFormServlet
 */
@WebServlet("/UpdateFormServlet")
public class UpdateFormServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   
		if(request.getSession(false) == null) {
			response.getWriter().println("please login ");
			return;
		}
		
	   int 	studentId = Integer.parseInt(request.getParameter("inp_id"));
	   Student student = new StudentServiceImpl().findById(studentId);
	   request.setAttribute("student",student);
	   request.getRequestDispatcher("UpdateStudent.jsp").forward(request, response);
	   
	}

	
}
