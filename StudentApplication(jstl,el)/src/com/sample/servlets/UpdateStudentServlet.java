package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class UpdateStudentServlet
 */
@WebServlet("/UpdateStudentServlet")
public class UpdateStudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  

	    if(request.getSession().getAttribute("username")== null) {
	    	RequestDispatcher rd = request.getRequestDispatcher("Error.jsp");
			request.setAttribute("msg", "Please login");
			rd.include(request, response);
	   		return;
	   	}
	    
	    
	    StudentService service = new StudentServiceImpl();
	    
	    int studentId = Integer.parseInt(request.getParameter("id").trim());
		String studentName = request.getParameter("name").trim();
		int studentAge = Integer.parseInt(request.getParameter("age").trim());
			
		Student student = new Student();
		student.setId(studentId);
		student.setName(studentName);
        student.setAge(studentAge);
		boolean status = service.updateStudent(student);
	    String msg ;
		if(status) {
			msg = "updated successfully";
		}
		else {
			msg = "update failed";
		}
		RequestDispatcher rd = request.getRequestDispatcher("Error.jsp");
		request.setAttribute("msg", msg);
		rd.include(request, response);
	    
	    
	}

}
