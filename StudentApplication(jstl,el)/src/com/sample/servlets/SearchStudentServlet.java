package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class SearchStudentServlet
 */
@WebServlet("/SearchStudentServlet")
public class SearchStudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String selection = request.getParameter("selector");
		
		if(request.getSession().getAttribute("username")== null) {
			RequestDispatcher rd = request.getRequestDispatcher("Error.jsp");
			request.setAttribute("msg", "please Login");
			rd.include(request, response);
	   		return;
	   	}
		
		StudentService service = new StudentServiceImpl();
		List<Student> studentList = new ArrayList<Student>();
		try {
			
			
		if(selection.equals("id")) {
			int studentId = Integer.parseInt( request.getParameter("input_name_id"));
		 	Student student = service.findById(studentId);
			if(student != null)
			    studentList.add(student);
			
		}
		else {
			String studentName =  request.getParameter("input_name_id");
		 	studentList = service.findByName(studentName);
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("ShowStudents.jsp");
		request.setAttribute("studentsList", studentList);
		rd.forward(request, response);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} 
	}

}
