package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class DeleteStudentServlet
 */
@WebServlet("/DeleteStudentServlet")
public class DeleteStudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   /**
    * this method deletes the student 
    * @param studentId
    * @return deletion status
    */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String msg ;
		if(request.getSession().getAttribute("username")== null) {
			RequestDispatcher rd = request.getRequestDispatcher("Error.jsp");
			request.setAttribute("msg", "Please login");
			rd.include(request, response);
	   		return;
	   	}
		
		
		
		int studentId = Integer.parseInt(request.getParameter("id"));

		StudentService service = new StudentServiceImpl();
		Student student = service.findById(studentId);
		if(student != null) {
			boolean result = service.deleteStudent(studentId);
			
			
			if(result) {
				msg = "Deleted successfully";
			}
			else {
				msg = "some thing went wrong...";
			}
			
		}else {
			msg = " No student found...";
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("Error.jsp");
		request.setAttribute("msg", msg);
		rd.include(request, response);

	}
	
	

}
