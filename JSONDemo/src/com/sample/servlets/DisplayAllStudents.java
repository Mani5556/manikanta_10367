package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jasper.tagplugins.jstl.core.Out;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class DisplayAllStudents
 */
@WebServlet("/DisplayAllStudents")
public class DisplayAllStudents extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   	
   		if(request.getSession().getAttribute("username")== null) {
   			RequestDispatcher rd = request.getRequestDispatcher("Error.jsp");
			request.setAttribute("msg", "Please Login");
			rd.include(request, response);
	   		return;
	   	}
	   		
   		StudentService service = new StudentServiceImpl();
   		List<Student> studentList = service.fetchAllStudents();
   		response.setContentType("Application/json");
//   		request.setAttribute("studentsList", studentList);
//   		RequestDispatcher rd = request.getRequestDispatcher("ShowStudents.jsp");
//   		rd.forward(request, response);
   		
   		Gson gson = new Gson();
   		String json = gson.toJson(studentList);
   		response.getWriter().println(json);
                          
   	}

}
