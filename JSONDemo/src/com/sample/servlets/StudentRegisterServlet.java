package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class StudentServlet
 */
@WebServlet("/StudentServlet")
public class StudentRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("username")== null) {
			RequestDispatcher rd = request.getRequestDispatcher("Error.jsp");
			request.setAttribute("msg", "Please Login");
			rd.include(request, response);
	   		return;
	   	}
		int studentId = Integer.parseInt(request.getParameter("id"));
		String studentName = request.getParameter("name");
		int studentAge = Integer.parseInt(request.getParameter("age"));
			
		Student student = new Student();
		student.setId(studentId);
		student.setName(studentName);
        student.setAge(studentAge);
		
		StudentService service = new StudentServiceImpl();
		boolean result = service.insertStudent(student);
				
		String msg;
		if(result) {
			msg = "Student Saved";
		}
		else {
			msg = "Some thing went wrong";
		}
		RequestDispatcher rd = request.getRequestDispatcher("Error.jsp");
		request.setAttribute("msg", msg);
		rd.include(request, response);
		
		
	}

}
