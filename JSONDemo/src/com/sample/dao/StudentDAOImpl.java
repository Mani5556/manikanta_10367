package com.sample.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.sample.bean.Student;
import com.sample.util.DBUtil;


public class StudentDAOImpl implements StudentDAO {
    /**
     * this method registers the student
     * @param student object
     * @return boolean
     */
	@Override
	public boolean createStudent(Student student) {
		
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "insert into student_tbl values( ?, ?, ?)";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, student.getId());
			ps.setString(2, student.getName());
			ps.setInt(3, student.getAge());
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected == 1) {
				result = true;
			} 
			
			
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
   /**
    * this method serched the student with id 
    * @param int studentntId
    * @return Student object
    */
	@Override
	public Student searchById(int id) {
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Student st = null;
		String sql = "select * from student_tbl where id = ?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while(rs.next()) {
				st= new Student();
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));
				
			}
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return st;
	}
  /**
   * thid method returns all  the students in the database 
   * @return lIST<Student>
   * 
   */
	@Override
	public List<Student> getAllStudents() {
		
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Student st = null;
		List <Student> studentList = new ArrayList<Student>();
		String sql = "select * from student_tbl order by id";
		try {
			con = DBUtil.getCon();
			stmt= con.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				st= new Student();
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));
				studentList.add(st);
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return studentList;
	}
 /**
  * this method searches the student who mathches  full name or subString of name 
  * @param search String 
  * @return List<Student>
  */
	@Override
	public List<Student> searchByName(String name) {
		
		Connection con = null;
		ResultSet rs = null;
		Student st = null;
		List <Student> studentList = new ArrayList<Student>();
		String sql = "select * from student_tbl where name like ?";
		try {
			con = DBUtil.getCon();
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, "%"+name+"%");
			rs = ps.executeQuery();
			while(rs.next()) {
				st= new Student();
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));
				studentList.add(st);
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return studentList;
	}
    /**
     * this method updates the student name and age 
     * @param Student object
     * @boolean
     */
	@Override
	public boolean updateStudent(Student student) {
			
			Connection con = null;
			PreparedStatement ps = null;
			boolean result = false;
			String sql = "update student_tbl set name = ?,age = ? where id = ?";
			try {
				con = DBUtil.getCon();
				ps = con.prepareStatement(sql);
				ps.setString(1, student.getName());
				ps.setInt(2, student.getAge());
				ps.setInt(3, student.getId());
				int rowsEffected = ps.executeUpdate();
				if(rowsEffected == 1) {
					result = true;
				} 
				
				
			}
			catch (SQLException e){
				e.printStackTrace();
			}
			finally {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			return result;
	}
    /**
     * this method delete the student from the database if given student id matches
     * @param int studentId
     * @return boolean
     */
	@Override
	public boolean deleteStudent(int studentId) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "delete from student_tbl where id = ?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1,studentId);
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected == 1) {
				result = true;
			} 
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	/**
	 * this method returns student new studentId 
	 * @return int 
	 * 
	 */
	
	public int getNewStudentId() {

		Connection con = null;
		Statement st = null;
		int studentId = 101;
		String sql = "select id from student_tbl order by id desc limit 1";
		try {
			con = DBUtil.getCon();
			st = con.createStatement();
			
		
			ResultSet rs = st.executeQuery(sql);
			if(rs.next()) {
				studentId = rs.getInt(1)+1 ;
			} 
			
			
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return studentId;
	}
}











