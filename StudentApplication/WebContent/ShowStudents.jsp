<%@page import="com.sun.xml.internal.bind.v2.schemagen.xmlschema.Import"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="com.sample.bean.*,java.util.*" %>
<%@ include file="home.html" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body >
<%
List<Student> students = (List<Student>)request.getAttribute("studentsList");
if(students.size() == 0){
	out.print("<h1 style ='color:red'> No students found</h1>");
	return;
}
%>
<center style="margin-top: 100px">
<table border="1">
<tr><th>Id</th><th>name</th><th>age</th></tr>
<%
for(Student student:students){
 %>
 <tr><td><%=student.getId() %></td><td><%=student.getName() %></td><td><%=student.getAge() %></td></tr>
 <%
 } 
 %>

</table>
 </center>
 </body>
</html>