<%@page import= "com.sample.service.*"%>
<%@page import= "com.sample.bean.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@include file="home.html" %>
<jsp:useBean id="service" class="com.sample.service.StudentServiceImpl"></jsp:useBean>
   
<!DOCTYPE html>
<html>
<head>
<link rel = "stylesheet" href="homestyle.css">
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

    <%
    if(request.getSession().getAttribute("username")== null) {
   		out.print("please login ...");
   		return;
   	}
   
    Student student = service.findById(Integer.parseInt(request.getParameter("inp_id")));
	if(student != null){
	%>
	
	<form action="UpdateStudentServlet">
	  StudentId:<input type="text" name = "id" readonly="readonly" value= "<%= student.getId()%> "><br><br>
	  Student name :<input type="text" name = "name" value= "<%= student.getName()%>"><br><br>
	  student Agg:<input type="text" name = "age" value= "<%= student.getAge()%>"><br><br>
	  <input type="submit" value = "update">
	
	</form>
	<%
	} 
	else{
		%>
		<h1 style="color: red">No student found</h1>
	<%
	   }
	%>

</body>
</html>