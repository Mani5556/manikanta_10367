
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ include file="home.html" %>
    <jsp:useBean id="service" class="com.sample.service.StudentServiceImpl"></jsp:useBean>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel = "stylesheet" href="homestyle.css">
<script>

   function validInt(){
	   let id = document.getElementById("age").value;
	   if(isNaN(id) || id.length == 0){
		 window.alert("invalid age");
		   
	   }else{
	       document.getElementById("form1").submit();
	   }
	   
   }

</script>
</head>
<body>
<%
  int studentId = service.getNewStudentId();
%>
<form id = "form1" action="StudentServlet" method="post">
<h1>Student Registration Form</h1>
<table >
 <tr><td>Student id:</td><td><input type="number" id = "id" name="id" value = "<%= studentId %>" readonly="readonly"/></td></tr>
 <tr><td>Student Name:</td><td><input type="text" name="name"/></td></tr>
 <tr><td>Student age:</td><td><input type="number" name="age" id = "age"/></td></tr>
 <tr><td></td><td><input type="button" value="save" onclick="validInt()"/></td></tr>
</table>
</form>
</body>
</html>