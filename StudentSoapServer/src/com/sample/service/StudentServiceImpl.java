package com.sample.service;

import java.util.List;

import com.sample.bean.Student;
import com.sample.dao.StudentDAO;
import com.sample.dao.StudentDAOImpl;

/**
 * This class is used for inserting, updating, displaying and deleting the
 * Student information
 * 
 * @author IMVIZAG
 *
 */

public class StudentServiceImpl implements StudentService{

	/**
	 * This method is used to insert the student data and returns the boolean result
	 * "true" if the student data is inserted
	 */
	
	@Override
	public boolean insertStudent(Student student) {
		// TODO Auto-generated method stub
		
		StudentDAO studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.createStudent(student);
		return result;
	}

	/**
	 * this method is used for searching the student data based on the student id
	 */
	
	@Override
	public Student findById(int id) {
		
		StudentDAO studentDAO = new StudentDAOImpl();
		Student student = studentDAO.searchById(id);
		return student;
	}

	/**
	 * this method is used for displaying all the students in the database
	 */
	
	@Override
	public List<Student> fetchAllStudents() {
		
		StudentDAO studentDAO = new StudentDAOImpl();
		List<Student> studentList = studentDAO.getAllStudents();
		return studentList;
	}

	/**
	 * this method is used for deleting the student data from the database
	 */
	
	@Override
	public boolean removeStudent(int id) {
		
		StudentDAO studentDao = new StudentDAOImpl();
		boolean result = studentDao.deleteStudent(id);
		return result;
	}

	/**
	 * this method is used for updating the student data from the database
	 */
	
	@Override
	public boolean updateStudent(Student student) {
		StudentDAO studentDao = new StudentDAOImpl();
		boolean result = studentDao.updateStudent(student);
		return result;
			
	}
}