package com.abc.util;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

public class MyGenerator implements IdentifierGenerator {
    String productId = null;
	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
	    try {
	    	Connection con = session.connection();
	    	Statement  st = con.createStatement();
	    	ResultSet rs = st.executeQuery("select COUNT(product_id) as count from product");
	    	while (rs.next()) {
				int count = rs.getInt("count");
				productId = "P"+(101 + count);
				
			}
	    }catch (Exception e) {
			e.printStackTrace();
		}
		return productId;
	}
    
}
