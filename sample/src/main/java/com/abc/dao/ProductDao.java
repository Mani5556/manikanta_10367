package com.abc.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.abc.entity.Product;
import com.abc.util.HibernateUtil;

public class ProductDao {

    public boolean create(Product product) {
    	SessionFactory  sessionFactory = new HibernateUtil().getSessionFactory();
    	Session sesson = sessionFactory.openSession();
    	Transaction transaction = sesson.beginTransaction();
    	String id = (String)sesson.save(product);
    	transaction.commit();
    	sesson.close();
    	System.out.println("object saved "+id);
    	return true;
    }
}
