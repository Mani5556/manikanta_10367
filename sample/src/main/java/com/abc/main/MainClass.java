package com.abc.main;

import java.security.Key;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.*;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.abc.dao.ProductDao;
import com.abc.entity.Product;
import com.abc.entity.Store;
import com.abc.services.ProductService;
import com.abc.util.HibernateUtil;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class MainClass {
   public static void main(String[] args) {
	   
	   Product product = new Product();
	   
	  // product.setProductId("p103");
	   product.setProductName("nokia");
	   product.setPrice(10000);
	   product.setCategory("mobile");
	   
	   //creating the one more product ibject
//	   Product product1 = new Product();
//	   product1.setProductName("nokia");
//	   product1.setPrice(12000);
//	   product1.setCategory("mobile");
//	
	   
//	   Store store = new Store();
//	   store.setLocation("hyd");
//	   store.setStoreName("mobile showroom");
//	   ArrayList<Product> products = new ArrayList<Product>();
//	   products.add(product);
//	  // products.add(product1);
//	   store.setProduct(products);
//	   
//	   ArrayList<Store> stores = new ArrayList<Store>();
//	   stores.add(store);
//	   product.setStore(stores);
//	  // product1.setStore(stores);
	   
	
	   SessionFactory sessionFactory = new HibernateUtil().getSessionFactory();
	   Session session = sessionFactory.openSession();
	   Store store =  session.get(Store.class, 1);
	   store.getProduct().add(product);
	   Transaction transaction = session.beginTransaction();

	  
	   
	   session.save(product);
	   //session.save(product1);
	   
	   session.save(store);
	   transaction.commit();
	   
	   
	   Store st = session.get(Store.class, 1);
	   if(st.getProduct().size() > 0) {
		   for(Product pro : st.getProduct())
		      System.out.println(pro.getProductName());
	   }
	   
	   
	   session.close();
	   //System.out.println(products.size() + " " + products.get(0).getProductName());
   }
	   
}
