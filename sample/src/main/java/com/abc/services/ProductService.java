package com.abc.services;

import com.abc.dao.ProductDao;
import com.abc.entity.Product;

public class ProductService {

	public boolean createProduct(Product product) {
	   return new ProductDao().create(product);	
	}
}
