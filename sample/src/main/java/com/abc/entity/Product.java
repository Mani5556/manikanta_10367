package com.abc.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name = "product")
public class Product {
    @Id
    @GenericGenerator(name ="myGenerator",strategy = "com.abc.util.MyGenerator")
    
    
    @GeneratedValue(generator = "myGenerator")
    @Column(name = "product_id")
	private String productId;
   
    @Column(name = "product_name")
    private String productName;
    
    @Column(name = "product_price")
    private double price;
    @Column(name = "catagory")
    private String category;
    
    @ManyToMany()
    private List<Store> store;
    
  
	/**
	 * @return the studentId
	 */
	public String getProductId() {
		return productId;
	}
	/**
	 * @param studentId the studentId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * @return the store
	 */
	public List<Store> getStore() {
		return store;
	}
	/**
	 * @param store the store to set
	 */
	public void setStore(List<Store> store) {
		this.store = store;
	}
	
	
}
